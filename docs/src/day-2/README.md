# Dzień Drugi

Witaj na drugim dniu warsztatów. Dzisiaj rozwiniemy bardzo to co robiliśmy wczoraj. Zaczniemy od implementacji wzorca repository aby przejść do definiowania relacji, kluczy i obsługi bardziej zaawansowanych przypadków użycia.

## Ćwiczenie 1 - Implementacja Repozytorium

- [ ] Utwórz klasę `OwnerRepository` w projekcie Dal
- [ ] W konstruktorze klasy `OwnerRepository` przekaż obiekt klasy `MyContext` 

::: tip
Dobrą praktyką jest sprawdzanie czy context nie jest null-em

```csharp
public Constructor(DbContext context)
{
    _context = context ?? throw new ArgumentNullException();
}
```

Szczególnie jest to ważne w takich aplikacjach, gdzie sami tworzymy graf zależności obiektów
:::

- [ ] Przypisz instancje obiektu `MyContext` do prywatnej zmiennej o nazwie `_context`
- [ ] W klasie `OwnerRepository` utwórz metody z następującymi nazwami / parametrami
  - [ ] `void Add(Owner owner)`
  - [ ] `void Update(Owner owner)`
  - [ ] `Owner Get(int ownerId)`
  - [ ] `List<Owner> Get(int[] ownersId)`
  - [ ] `void Delete(Owner owner)`
- [ ] Napisz implementację każdej metody bazując na prezentacji i wczorajszych przykładach

::: tip
Pamiętaj aby na końcu każdej metody (która zmienia stan bazy danych) wywołać `_context.SaveChanges()`
:::

::: tip
Dla metody update możesz przed wywołaniem metody `SaveChanges` ustawić stan tej encji an zmodyfikowaną za pomocą kodu

```csharp
_context.Entry(owner).State = EntityState.Modified;
```

Samymi stanami encji zajmiemy się trochę póżniej
:::

- [ ] W pliku `Program.cs` utwórz instancję klasy `OwnerRepository`
- [ ] Przetestuj wszystkie metody klasy `OwnerRepository` czy działają poprawnie.

## Ćwiczenie 2 - Generyczne Repozytorium

W wielu przypadkach kiedy tworzymy systemy opartę na 4 akacjach CRUD (Create, Read, Update, Delete) to nie ma potrzeby aby dla każdej encji pisać osobne repozytorium. Dużo lepszym rozwiązaniem będzie stworzenie generycznego repozytorium, która pokryje nam właśnie te 4 podstawowe scenariusze.

- [ ] W projekcie Dal utwórz klasę `BaseEntity`
- [ ] Dodaj do klasy `BaseEntity` propertis `int Id`
- [ ] Zaktualizuj klasę `Owner` aby dziedziczyła po klasie `BaseEntity`
- [ ] W projekcie Dal utwórz klasę `Repository` z generycznym 
- [ ] Do klasy Repository dodaj parametr generyczny T, który musi dziedziczyć po klasie `BaseEntity`
- [ ] W klasie `Repository` zdefiniuj metody: 
  - [ ] `void Add(T entity)`
  - [ ] `void Update(T entity)`
  - [ ] `T Get(int entityId)`
  - [ ] `List<T> Get(int[] ownersId)`
  - [ ] `void Delete(T owner)`
- [ ] W pliku `Program.cs` utwórz instację klasy `Repository<Owner>`

## Ćwiczenie 3 - Relacje 1:1

Otrzymaliśmy nowe wymaganie biznesowe do zaimplementowane w naszym systemie kliniki weterynaryjnej. Okazało się że potrzebujemy przechowywać adresy właścicieli psów. 
Wstępnie właściciel kliniki powiedział że właściciel będzie mógł posiadać od 0 - 1 adresów przypisanych do jego konta.

Poniższy diagram obrazuje schemat bazy danych, który chcemy osiągnąć w takim przypadku.

```mermaid
erDiagram
    OWNER ||--o| ADDRESS : have
    OWNER {
        int Id
        string FirstName
        string LastName
        string Note
    }
    ADDRESS {
        string City
        string Street
        string ZipCode
    }
```

- [ ] Zacznij od stworzenia klasy `Address` w projekcie Dal. Na chwilę obecną ta klasa nie musi dziedziczyć po klasie `BaseEntity`
- [ ] Następnie zdefiniuj wszystkie pola dla klasy `Address` dodając też pole `Id`
- [ ] Stwórz klasę `AddressEntityTypeConfiguration` w której będziesz przechowywać konfigurację klasy `Address` zgodną z poniższymi założeniami
    - [ ] Tabela `Address` ma zostać utworzona w Schemie `MyVet`
    - [ ] Pole City ma być wymagane
    - [ ] Pole Street ma być wymagane
    - [ ] Pole ZipCode ma być wymagane
    
::: tip
Jako że nie będziemy się odwoływac do Adressu bez zaciągania encji Owner-a to nie ma potrzeby tworzenia `DbSet<Address>` w klasie `MyContext`
:::

- [ ] Do klasy `Owner` dodaj pole `Address`

::: tip
Bardzo często w systemach IT, w momencie kiedy zachodzą wieksze zmiany stosuje się pola opcjonalne. Pozwala to na pracę na "starych" danych, które mogą już nie spełniać wymagań biznesowych. 
Więcej na temat migracji danych możesz się dowiedzieć z ksiązki [Refactoring Databases](https://www.amazon.com/Refactoring-Databases-Evolutionary-paperback-Addison-Wesley/dp/0321774515)
:::

- [ ] Następnie przejdź do klasy `Address` i dodaj tam pola
    - [ ] int OwnerId
    - [ ] Owner Owner
- [ ] Przejdź do klasy `OwnerEntityTypeConfiguration` i napisz konfigurację relacji zgodną z fluentApi (przykład był pokazany w części teoretycznej)

::: tip
Jako że nie definiujemy propertisa: 
```csharp
int? Address { get; set; }
```
to potrzebujemy użyć fluentApi dostarczanego przez `IEntityTypeConfiguration`. Dzięki temy nasz model jest "czystszy" bo nie mamy nadmiarowego propertis-a, jednak to jest za cenę tej konfiguracji.
:::

::: details Rozwiązanie
```csharp
builder.HasOne(x => x.Address)
    .WithOne(x => x.Owner)
    .HasForeignKey<Address>(x => x.OwnerId);
```
:::

::: warning
Korzystając z generycznego repozytorium dość cieżko jest wymusić załadowanie elementów z relacji ponieważ nie mamy możliwości dodania: 

```csharp
_context.Owners
  .Include(x => x.Dogs)
  .Include(x => x.Address)
  .Find(ownerId);
```

Dlatego w przypadku bardziej zaawansowanych obiektów, lepiej jest tworzyć dedykowane repozytorium dla encji niż starać się wymusić na EF Core ładowanie wszystkich zależności.
:::

- [ ] Przetestuj napisany kod dodając adres to jakiegoś właściciela. Zobacz jakie zapytania są generowane przez EF Core

## Ćwiczenie 4 - Relacje 1 - M

W momencie kiedy juz kończyłeś pisać obsługę adres-ów w aplikacji pojawił się człowiek z biznesu. Po krótkiej rozmowie okazało się że plany zostały nieco zmienione, ponieważ rozważają wprowadzenie usługi wysyłania leków do właścicieli. Dlatego też każdy właściciel powienien mieć możliwość zdefiniowania kilku adresów.

Niestety zmiana jest jedynym pewnym punktem w procesie wytwarzania oprogramowania. Musimy zedytować kod tak aby struktura bazy danych wyglądała następująco: 

```mermaid
erDiagram
    OWNER ||--o{ ADDRESS : have
    OWNER {
        int Id
        string FirstName
        string LastName
        string Note
    }
    ADDRESS {
        string City
        string Street
        string ZipCode
    }
```

- [ ] Przejdź do klasy `Owner` i zmień definicję propertis-a `Address` na `ICollection<Address>`
- [ ] Zdefiniuj też bez parametrowy konstruktor klasie `Owner` i zainicjalizuj w nim pustą listę `Address`
- [ ] Następnie korzystając z składni pokazanej na części teoretycznej uaktualnij konfigurację `OwnerEntityTypeConfiguration` aby obsługiwało relację 1 - M 
- [ ] Do instancji obiektu `Owner` utworzonej w pliku `Program.cs` dodaj kilka adres-ów
- [ ] Uruchom aplikację zobacz jakie zapytania generuje EF Core do bazy danych
- [ ] Spróbuj zmienić wartość `OnDelete` na inne opcję i zobacz jaki wpływ będzie to miało na baze danych i EF Core Mozesz wypróbować takie opcję jak <Badge text="Dla Chętnych"/>
    - [ ] DeleteBehavior.SetNull
    - [ ] DeleteBehavior.Restrict
    - [ ] DeleteBehavior.ClientCascade
    
## Ćwiczenie 5 - Relacja M : M

Przechodzimy do ostatniego typu relacji, relacja `Many to Many`. Jest to nowość w Entity Framework Core, ponieważ została wprowadzona dopiero w EF Core 5. Wcześniejsza wersja Entity Framework oparta o platformę .NET Framework miała taki mechanizm wbudowany. 

Ponownie w biurze pojawia się człowiek odpowiedzialny za kontakty z biznesem. Doskonale wiesz co się będzie działo... Zmiany! Po chwili rozmowy dowiadujesz się że klienci chcą dodać "małą" modyfikację do naszego systemu. Mianowicie okazało się że w wielu przypadkach nie ma pojedyńczego właściciela dla jednego czworonoga. Dlatego też zostałeś poproszony o wprowadzenie zmian aby do jednego właściciela dało się przypisać wiele psów, ale także aby do jednego psa dało się przypisać wielu właścicieli.

Z lotu ptaka, taka relacja będzie wyglądała następująco: 

```mermaid
erDiagram
    OWNER }o--o{ DOG : have
    OWNER {
        int Id
        string FirstName
        string LastName
        string Note
    }
    DOG {
        int DogNumber
        string Name
    }
```

O ile na wykresie bardzo łatwo przedstawić taką relację to w kodzie i na poziomie bazy danych musimy się wspomagać trochę innymi rozwiązaniami. Dlatego tutaj będziemy chcieli użyć tabeli pośredniczącej aby przechować "łączenia". 

```mermaid
erDiagram
    OWNER ||--o{ DogOwner : have
    DOG ||--o{ DogOwner : have
    OWNER {
        int Id
        string FirstName
        string LastName
        string Note
    }
    DogOwner {
        int OwnerId
        int DogNumber
    }
    DOG {
        int DogNumber
        string Name
    }
```

- [ ] Przejdź do klasy `Dog` i usuń propertis `OwnerId`
- [ ] W klasie `Dog` zmień definicję `Owner` na `ICollection<Owner>`
- [ ] W klasie konfiguracyjnej `DogEntityTypeConfiguration` usuń wzmiankę o `OwnerId`

::: tip
Jako że tutaj definiujemy relację M:M za pomocą navigation property to nie potrzebujemy konfigurować jej w żadnym `EntityTypeConfiguration`. EF Core uzna to za relację M:M przez wbudowaną konwencję. 
:::

Po tych kilku zmianach jeżeli wszystko zadziałało poprawnie powineneś móc ujrzeć w bazie danych następującą strukturę: 

![db-schema](./assets/dogowner-db-schema.png)

## Ćwiczenie 6 - Relacja M : M - Encja łącząca

Bardzo często się zdarza że w momencie kiedy mamy relację M : M to oprócz samych idk-ów chcemy przechować też inne dane. Nie inaczej jest tym razem. Człowiek od komunikacji z biznesem ponownie zawitał do Twojego biura, mówiąc że biznes potrzebuje mieć informację o tym kiedy powstała relacja między psem a właścicielem. 

Szybkie zmiany na diagramie bazy danych i już masz nowy model przed sobą

```mermaid
erDiagram
    OWNER ||--o{ DogOwner : have
    DOG ||--o{ DogOwner : have
    OWNER {
        int Id
        string FirstName
        string LastName
        string Note
    }
    DogOwner {
        int OwnerId
        int DogNumber
        DateTime CreatedAt
    }
    DOG {
        int DogNumber
        string Name
    }
```

::: tip
W tym miejscu można by było się pokusić o refaktoryzację w celu osiągnięcia DSL (Domain Specific Language) ponieważ bycie właścicielem to nazwa relacji jaką ma osoba z psem. Dlatego prawdopodobnie lepiej by było stworzyć poniższy model: 

```mermaid
erDiagram
    Person ||--o{ Owner : have
    Dog ||--o{ Owner : have
    Person {
        int Id
        string FirstName
        string LastName
        string Note
    }
    Owner {
        int OwnerId
        int DogNumber
        DateTime CreatedAt
    }
    Dog {
        int DogNumber
        string Name
    }
```

W celu zapoznania się z ideą języka domenowego polecam książke Eric-a Evens-a [Domain Driven Desing](https://www.amazon.com/Domain-Driven-Design-Tackling-Complexity-Software/dp/0321125215)
:::

Aby to zrobić będziesz musiał wykorzystać fluent API, możesz to zrobić albo w metodzie `OnConfigure` w klasie `MyContext` albo w klasie `OwnerEntityTypeConfiguration`.

- [ ] Utwórz klasę `DogOwner` w projekcie Dal
- [ ] W kalsie `DogOwner` zdefiniuj następująco propertisy
    - [ ] `Datetime CreatedAt`
    - [ ] `int DogId`
    - [ ] `Dog Dog`
    - [ ] `int OwnerId`
    - [ ] `Owner Owner`
- [ ] Przejdź do klasy `Owner` i dodaj propertis `List<DogOwner> DogOwners`
- [ ] Przejdź do klasy `Dog` i dodaj propertis `List<DogOwner> DogOwners`
- [ ] Przejdź do klasy `OwnerEntityTypeConfiguration` i dodaj konfigurację relacji M:M uzywając encji łączącej

::: tip
Aby wymusić generowanie wartości CreatedAt po stronie bazy danych możesz wykorzystać kod: 

```csharp
x => x.Property(x => x.Props)
    .HasDefaultValueSql("CURRENT_TIMESTAMP");
```
:::

::: details Rozwiązanie
```csharp
builder.HasMany(x => x.Dogs)
    .WithMany(x => x.Owner)
    .UsingEntity<DogOwner>(
        d => d.HasOne(x => x.Dog)
                .WithMany(x => x.DogOwners)
                .OnDelete(DeleteBehavior.Cascade), 
        o => o.HasOne(x => x.Owner)
            .WithMany(x => x.DogOwners)
            .OnDelete(DeleteBehavior.Cascade),
        dogOwner =>
        {
            dogOwner.Property(x => x.CreatedAt)
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
        }
    );
```
:::

## Ćwiczenie 7 - Relacje - Owned Types

- [ ] Utwórz klasę `Email` w projekcie Dal
- [ ] W klasie email stwórz propertis `Value`, który będzie miał prywatny setter
- [ ] Ustaw wartość propertis-a `Value` przez konstruktor
- [ ] Przejdź do klasy `Owner` i dodaj propertis do niego o nazwie i typie `Email`
- [ ] W klasie `OwnerEntityTypeConfiguration` skonfiguruj pole `Email` wykorzystując mechanizm `OwnsOne` tak aby tabela Owners wyglądała następująco
```mermaid
erDiagram
    OWNER {
        int Id
        string FirstName
        string LastName
        string Note
        string Email
    }
```
- [ ] Uruchom aplikację i sprawdź czy email zapisuje się poprawnie

::: details Rozwiązanie
```csharp
builder.OwnsOne(x => x.Email, e =>
{
    e.Property(x => x.Value)
        .HasColumnName(nameof(Email))
        .HasMaxLength(255);
});
```
:::

- [ ] Utwórz klasę `Toy` w projekcie Dal
- [ ] W klasie `Toy` zdefiniuj takie pola jak
    - [ ] `string Name`
    - [ ] `string Producer`
    - [ ] `DateTime CreatedAt`
- [ ] W klasie `Dog` dodaj propertis `List<Toy> Toys`
- [ ] W klasie `DogEntityTypeConfiguration` za pomocą mechanizmu `OwnsMany` dodaj konfigurację dla listy zabawek
```mermaid
erDiagram
    Dog ||--o{ Toy : having
    Dog {
        int DogNumber
        string Name
    }
    Toy {
        int Id
        string Name
        string Producer
        DateTime CreatedAt
    }
```
- [ ] Uruchom aplikację i sprawdź czy zabawki zapisują się poprawnie

::: details Rozwiązanie
```csharp
builder.OwnsMany(x => x.Toys, t =>
{
    t.HasKey(x => x.Id);

    t.Property(x => x.Name);
    t.Property(x => x.Producer);
    t.Property(x => x.CreatedAt);

    t.ToTable("Toys", MyContext.Schema);
});
```
:::

## Ćwiczenie 8 - Shadow Property (Właściwości cienia :smile:)

- [ ] Przejdź do klasy `OwnerEntityTypeConfiguration`
- [ ] Następnie dodaj konfiguracje dla pola UpdatedAt, które nie jest zdefiniowane w klasie `Owner`

::: tip
```csharp
builder.Property<DateTime>("Name")
```
:::

- [ ] Ustaw wartość Shadow Property na `CURRENT_TIMESTAMP` za pomocą metody `HasDefaultValueSql`
- [ ] Uruchom aplikację i przetestuj zmiany

## Ćwiczenie 9 - Backing Fields i Enkapsulacja List

Jednym z filarów programowania obiektowe jest enkapsulacja. Na chwilę obecną dość dużo propertis jest publicznych. Jednak jak dowiedziałeś się z prezentacji EF Core nie ma problemów z ustawianiem wartości propertis-ów za pomocą reflekcji (o ile nie są one readonly). Trochę bardziej skomplikowana sytuacja jest z listami. Ponieważ nawet jeżeli setter listy będzie prywatny to nadal każdy będzie mógł modyfikować zawartość listy.

Poniższy kod jest całkowicie poprawny. 
```csharp
public class A {
    public List<string> List { get; set;}
}

var a = new A()
a.List.Add("");
```

Aby osiągnąć enkapsulacje list musimy użyć mechanizmu backing fields, który był przedstawiony w części teoretycznej: 

```csharp
public class A {
    private List<string> _list = new ();
    public IReadOnlyList<string> List => _list.AsReadonly();
}

var a = new A();
a.List.Add(""); // Teraz ta linijka rzuci błędem
```

- [ ] Przejdź do klasy `Owner`
- [ ] Zmień implementację listy adresów aby używała mechanizmu backing fields
- [ ] Dodaj metodę w klasie `Owner` pozwalającą na dodanie nowego adresu do właściciela
- [ ] Dostosuj kod w pliku Program.cs aby dodawać listę za pomocą metody
- [ ] Uruchom aplikację i sprawdź czy wszystkie dane zapisują się poprawnie

## Ćwiczenie 10 - Indeksy

Ponownie do Twojego biura zawitał człowiek od komunikacji z biznesem i oznajmił że pojawiło się nowe wymaganie biznesowe. Każdy właściciel zwierzata musi mieć unikalne imie i nazwisko. Oznacza to że musisz wprowadzić ograniczenia na poziomie danych ponieważ w momencie kiedy aplikacja będzie miała wielu użytkowników to istnieje możliwosć że dwóch lub więcej uzytkowników o identycznych imionach i nazwiskach zarejesrtują się w tym samym czasie.

- [ ] Przejdź do klasy `OwnerEntityTypeConfiguration`
- [ ] Zdefiniuj index złożony, który musi być unikalny dla pary FirstName i LastName
- [ ] Uruchom aplikację i sprawdź czy indeks został dodany na poziomie baze danych
- [ ] Przejdź do klasy `MyContext` i nadpisz metodę `SaveChanges`
- [ ] W metodzie `SaveChanges` zamknij wywołanie `base.SaveChanges` w blok try catch
- [ ] W bloku catch sprawdź czy złapany wyjątek jest naruszeniem klucza `IX_Owners_FirstName_LastName`
- [ ] Jeśli tak to rzuć customowym wyjątkiem `FirstNameLastNameUniqueConstrainViolation`

::: tip
Implementację tego wyjątku możesz skopiować z tego miejsca: 

```csharp
public class FirstNameLastNameConstrainViolation : Exception
{
    public FirstNameLastNameConstrainViolation() : base()
    {
    }

    public FirstNameLastNameConstrainViolation(string message) : base(message)
    {
    }

    public FirstNameLastNameConstrainViolation(string message, Exception innerException) : base(message, innerException)
    {
    }
}
```
::: 

::: details Rozwiązanie
```csharp
public override int SaveChanges()
{
    try
    {
        return base.SaveChanges();
    }
    catch (Exception e)
    {
        static bool IsConstrainViolation(Exception e, string constrainName)
            => e.InnerException != null && e.InnerException.Message.Contains(constrainName);

        if (IsConstrainViolation(e, "IX_FirstName_LastName"))
            throw new FirstNameLastNameConstrainViolation();
        
        throw;
    }
}
```
:::

## Ćwiczenie 11 - Konwersje

#### Używanie wbudowanych konwersji

Ponownie analityk biznesowy pojawia się u Ciebie w biurze i prosi o kolejną zmianę. Jako że ostatnio było dość dużo przypadków fałszywych kont to prosi Ciebie o zaimplementowanie kilku statusów użytkowników: 
- Aktywny
- Oczekujący na aktywacje
- Zbanowany

Wiesz że EF Core bez problemów sobie zmapuje te statusy w momencie kiedy tylko zdefiniujesz sobie enum-a w aplikacji

```csharp
public enum OwnerState {
    Active,
    Pending,
    Banned
}
```

Jednak problemem jest to żę EF Core z default-u zmapuje te wartości do int-ów. Nie jest to złe jednak, nam bardziej zależny na czytelności danych niż na wydajności, dlatego fajnie byłoby mappować stan właściciela jako string.

- [ ] W projekcie Dal dodaj enum `OwnerState`
- [ ] W klasie `Owner` dodaj propertis `OwnerState State`
- [ ] Następnie w klasie `OwnerEntityTypeConfiguration` dodaj konfigurację pola state używając konwertera wartości

#### Własny konwerter wartości

Przychodzi kolejne wymaganie biznesowe. Tym razem pies oprócz swojej nazwy ma mieć listę "przydomków". Podobno to dobrze działa na właścicieli jak widzą obok nazwy swojego zwierzata jakiś przydomek, który mu nadali. 

Nasza baza danych powoli puchnie, jeżeli nadal będziemy dodawać tabele tylko po to aby przechowywać jakieś pojedyńcze wartości to zaraz skończymy z olbrzymią liczbą tabel, zależności, relacji etc. Dlatego nie ma potrzeby aby dodawać kolejny byt skoro możemy po prostu dodać kolejną kolumnę, która będzie przechowywać serializowaną listę. Przecież nie będziemy po tych przydomkach przeszukiwać tabeli.

- [ ] Przejdź do klasy `Dog`
- [ ] Następnie dodaj propertis `List<string> NickNames`
- [ ] Przejdź do klasy `DogEntityTypeConfiguration`
- [ ] Wewnątrz pliku z `DogEntityTypeConfiguration` dodaj Converter o nazwie `ListToStringConverter`
- [ ] Zaimplementuj `ListToStringConverter` wykorzystując `JsonConvert.SerializeObject`
- [ ] Skonfiguruj propertis `NickNames` z wykorzystaniem nowo stworoznego converter-a
- [ ] Uruchom aplikację i przetestuj czy lista przydomków zwierzaka zapisuje się poprawnie

::: details Rozwiązanie Converter
```csharp
public class ListToStringConverter : ValueConverter<List<string>, string>
{
    public ListToStringConverter() : base(x => Make(x), x => UnMake(x))
    {
    }
    
    static string Make(List<String> list) => JsonConvert.SerializeObject(list);
    static List<string> UnMake(string value) => JsonConvert.DeserializeObject<List<string>>(value);
}
```
:::

## Ćwiczenie 12 - Zapytania SQL

Istnieją sytuację kiedy możliwości EF Core nie pokrywają nam scenariusza, który chcemy zaimplementować. Zdarza się to w momentach kiedy chcemy zyskać na wydajności, lub mamy to zrobienia jakieś bardzo skomplikowane zapytanie. 
Właśnie na potrzeby takich sytuacji mamy do dyspozycji wywoływanie kodu Sql zdefiniowanego przez nas.

```csharp
var blogs = context.Blogs
    .FromSqlRaw("SELECT * FROM dbo.Blogs")
    .ToList();
```

- [ ] Przejdź do pliku `Repository`
- [ ] W klasie repository zdefiniuj metodę `GetAllToys`
- [ ] Następnie za pomocą metody `FromSqlInterpolated` pobierz wszystkie zabawki
- [ ] Wywołaj tą metodę w pliku `Program.cs` 
- [ ] Uruchom aplikację aby sprawdzić czy wszystko działa poprawnie

## Ćwiczenie 13 - Zapytania SQL - Procedury Składowane

Ostatnie ćwiczenie dzisiaj będzie polegało na wywołaniu procedury składowanej.
Możesz albo zdefiniować własną procedurę albo użyć poniższej: 

```sql
USE  EFCoreBasicWorkshop;
GO;

IF OBJECT_ID('MyVet.CountAllResources', 'P') IS NOT NULL
    DROP PROC MyVet.CountAllResources
GO

CREATE PROCEDURE MyVet.CountAllResources
AS
BEGIN
    SET NOCOUNT ON;
    SELECT
           (SELECT COUNT(*) from MyVet.Toys) as Toys,
           (SELECT COUNT(*) from MyVet.Owners) as Owners,
           (SELECT COUNT(*) from MyVet.Dogs) as Dogs
END
```

Jako że jeszcze nie mamy zaimplementowanych migracji to w pliku `Program.cs` możesz dodać następujący kod, który zawsze po `EnsureCreated` będzie dodawał procedurę składową do bazy danych: 

```csharp
context.Database.ExecuteSqlRaw(@"
CREATE PROCEDURE MyVet.CountAllResources
AS
BEGIN
    SET NOCOUNT ON;
    SELECT
           (SELECT COUNT(*) from MyVet.Toys) as Toys,
           (SELECT COUNT(*) from MyVet.Owners) as Owners,
           (SELECT COUNT(*) from MyVet.Dogs) as Dogs
END
");
```

Jest to bardzo prosta procedura składowana, której zadaniem jest zliczanie kilku encji.

- [ ] W projekcie Dal utwórz klasę `AllResourceCount`
- [ ] Udekoruj tą klasę atrybutem `Keyless`
- [ ] W tej klasie utwórz trzy propertisy
    - [ ] `int Toys`
    - [ ] `int Owners`
    - [ ] `int Dogs`
- [ ] Przejdź do klasy `MyContext`
- [ ] Dodaj propertis `DbSet<AllResourceCount> ResourceCount`
- [ ] Przejdź do klasy repozytorium
- [ ] W klasie repozytorium utwórz metodę `CountAllResources`, która będzie zwracała obiekt klasy `AllResourceCount`
- [ ] Zaimplementuj wywołanie procedury składowej zgodnie z tym co było pokazane w części teoretycznej

::: tip
Pamiętaj aby po ExecuteRawSql użyć ToList() zanim zaczniesz używać innych metod Linq takich jak First lub Single
:::

- [ ] Dodaj wywołanie tej funkcji w pliku `Program.cs`
- [ ] Uruchom aplikację i zobacz czy wszystko zadziałało prawidłowo

## Ćwiczenie 14 - Mapowanie do widoków <Badge text="Dla Chętnych"/>

Widoki na bazach danych bardzo często się stosuje aby zoptymalizować zapytania. EF Core ma bardzo fajny mechanim mapowania struktur danych do widoków.

Załóżmy że potrzebujemy mechanizmu tylko do wyświetlenia listy właścicieli psów { id, name }. Aby nie pisać zawsze 
```
list.Select(x => new { x.Id, x.Name })
```
to możemy skonfigurować widok na bazie danych.

Zacznijmy w pierwszej kolejności od kodu SQL

```sql
CREATE VIEW [MyVet].[Owners.OnlyIdAndName] AS
SELECT Id, FirstName
FROM MyVet.Owners
```

- [ ] Stwórz widok w pliku `Program.cs` przy pomocy `context.Database.ExecuteSqlRaw` tak jak to zrobiłeś z procedurą składowaną w poprzednim 
- [ ] W projekcie Dal utworz klasę `OwnerIdAndNameView` zawierającą dwa propertisy
    - [ ] int Id
    - [ ] string FirstName
- [ ] Utwórz klasę `OwnerIdAndNameViewConfiguration` implementującą interfejs `IEntityTypeConfiguration`
- [ ] W metodzie configure napisz konfigurację dla propertis-ów klasy `OwnerIdAndNameView`
- [ ] Na zakończenie metody configure dodaj kod: 
```csharp
builder.ToView("Owners.OnlyIdAndName", MyContext.DefaultSchema);
```
- [ ] Następnie przejdź do klasy `MyContext` i dodaj DbSet z parametrem generycznym `OwnerIdAndNameView`
- [ ] W pliku `Program.cs` pobierz dane z nowo utworzonego DBSet-a. Przy okazji zobacz jak zmieniają się zapytania kiedy próbujesz dodać do zapytania jakieś filtrowanie, warunki przez metody LINQ.
