# Dzień Pierwszy

Witaj w pierwszym dniu warsztatów, podczas tego dnia przejdziemy przez wstępną konifgurację biblioteki EntityFramework Core, utworzymy pierwszy model danych a także zobaczymy w jaki sposób można pobierać i zapisywać dane do bazy danych. 

## Ćwiczenie 1 - Przygotowanie projektu testowego

Zacznijmy od rzeczy całkowicie podstawowej, od utworzenia projektu na którym będziemy pracowali przez resztę tego warsztatu. Projekt można utworzyć na kilka sposób. Np. możesz do tego użyć menu znajdującego się w Visual Studio / Rider-ze pod tym [linkiem](https://docs.microsoft.com/en-us/visualstudio/ide/creating-solutions-and-projects?view=vs-2019) znajdziesz tutorial Microsoft-u jak to zrobić. 

Ja poniżej przedstawię Ci inny sposób, który także został zaprezentowany na zajęciach. Czyli utworzenie projektu za pomocą CLI. 
Do tego sposoby będziesz potrzebować konsoli / terminala. Możesz wykorzystać np. Git Bash.

Przejdź do lokacji gdzie chcesz przechowywać projekt. Kliknij prawym przyciskiem myszy i z rozwijanego menu wybierz opcję "Uruchom powłokę GitBash tutaj" (Ewentualnie możesz po prostu uruchomić powłokę Git Bash i przejść do miejsca docelowego za pomocą komendy `cd`).

W kolejnym kroku musisz wpisać kilka komend

```bash
$ dotnet new console -n EFCoreWorkshop.App -o App
$ dotnet new classlib -n EFCoreWorkshop.Dal -o Dal
$ dotnet new sln -n EFCoreWorkshop
$ dotnet sln add **/*csproj
``` 

Dodatkowo można od razu wygenerować plik `.gitignore` tak aby w repozytorium nie wylądowały niepotrzebne pliki. Dotnet CLI także zawiera komendę do wygenerowanie predefiniowanego pliku `.gitignore`. Trzeba ją wywołać w katalogu na poziomie solucji.

```bash
$ dotnet new gitignore
```

Jeżeli wszystkie komendy zakończyły się powodzeniem to wygenerowana struktura plików i folderów powinna wyglądać następująco

```
.
├── App
│   ├── EFCoreWorkshop.App.csproj
│   ├── Program.cs
│   └── obj
│       ├── EFCoreWorkshop.App.csproj.nuget.dgspec.json
│       ├── EFCoreWorkshop.App.csproj.nuget.g.props
│       ├── EFCoreWorkshop.App.csproj.nuget.g.targets
│       ├── project.assets.json
│       └── project.nuget.cache
├── Dal
│   ├── Class1.cs
│   ├── EFCoreWorkshop.Dal.csproj
│   └── obj
│       ├── EFCoreWorkshop.Dal.csproj.nuget.dgspec.json
│       ├── EFCoreWorkshop.Dal.csproj.nuget.g.props
│       ├── EFCoreWorkshop.Dal.csproj.nuget.g.targets
│       ├── project.assets.json
│       └── project.nuget.cache
└── EFCoreWorkshop.sln
```

Ostatnim elementem tego ćwiczenia jest dodanie referecji projektu Dal do projektu App. To także można zrobić w prosty sposób za pomocą konsoli. W konsoli przejdź do folderu, w którym znajduje się projekt App i następnie wywołaj komendę

```bash
$ dotnet add reference ../Dal/EFCoreWorkshop.Dal.csproj 
```

Pierwsze ćwiczenie zakończone super! Dobra robota!
![Great Job](https://media1.giphy.com/media/kigLtfDrV3K9N0wYCO/giphy.gif)

## Ćwiczenie 2 - Dodanie biblioteki Entity Framework Core do projektu DAL

Jak już zdefiniowaliśmy nasze projekty to możemy przejść do dodania biblioteki EntityFramework Core za pomocą package managera-a [nuget](https://www.nuget.org/). 
Jeżeli wejdziesz na stronę [nuget.org](https://nuget.org) i wyszukasz `EntityFrameworkCore` to zobaczysz statystyki ile pobrań ma ta biblioteka.

![Entity Framework Download Count](./assets/ef-core-download-count.png)
\* Stan na 04.09.2021

Aby dodać tą bibliotekę do aplikacji możesz skoczystać z UI Package Manager-a dodanego do Visual Studio / Rider lub zrobić to za pomocą CLI. Jak to zrobić w Visual Studio jest przedstawione w tutorialu na stronie Microsoft: [tutaj](https://docs.microsoft.com/en-us/nuget/quickstart/install-and-use-a-package-in-visual-studio)

Do projektu DAL trzeba dodać paczki: 
- Microsoft.EntityFrameworkCore.SqlServer
- Microsoft.Extensions.Logging.Console

Natomiast do projektu App trzeba dodać
- Microsoft.EntityFrameworkCore.Design

Jeżeli chcesz to zrobić za pomocą konsoli to musisz najpierw przejść do katalogu, w którym znajduje się projekt Dal. Następnie uruchomić komendy

```bash
$ dotnet add package Microsoft.EntityFrameworkCore.SqlServer
$ dotnet add package Microsoft.Extensions.Logging.Console
```

W kolejnym kroku musisz przejść do projektu App i wywołać polecenie

```bash
$ dotnet add package Microsoft.EntityFrameworkCore.Design
```

Po wykonaniu pierwszego polecenia z instalacją nuget-a EntityFrameworkCore.SqlServer będziesz mógł zobaczyć w konsoli że ta paczka dodała od razu kilka innych paczek, które EntityFrameworkCore będzie potrzebował do działania. 
Na chwilę obecną paczkami dodawanymi podczas instalacji EntityFrameworkCore są: 
- Microsoft.EntityFrameworkCore.Analyzers
- Microsoft.EntityFrameworkCore.Abstractions
- Microsoft.EntityFrameworkCore
- Microsoft.EntityFrameworkCore.Relational
- Microsoft.EntityFrameworkCore.SqlServer

### Instalacja globalnego narzędzia dotnet ef

Do sterowania Entity Framework Core najczęściej używa się narzędzia CLI dotnet ef. Dlatego warto je zainstalować globalnie. Wystarczy do tego jedno polecenie CLI

```bash
$ dotnet tool install --global dotnet-ef
```

::: warning
Pamiętaj aby aktualizować biblioteki globalne w przypadku aktualizacji paczek EF Core lub migracji na wyższą wersję platformy .NET
:::

### Generowanie manifestu z dodatkowymi narzędziami wykorzystywanymi w projekcie <Badge text="Dla Chętnych"/>

.NET pozwala nam na wygenerowanie manifestu, który będzie zawierał listę wszystkich wykorzystywanych narzędzi w projekcie. Przydaje się w szczególności kiedy wykorzystujemy narzędzia lokalne.

Aby wygenerować taki plik wystarczy w konsoli, bedą na poziomie solucji wpisać komendę: 

```bash
$ dotnet new tool-manifest
```

Po wykonaniu tej komendy do projektu zostanie dodany katalog `.config`.

```{2-3}
.
├── .config
│   └── dotnet-tools.json
├── App
│   ├── EFCoreWorkshop.App.csproj
│   ├── Program.cs
│   └── obj
│       ├── EFCoreWorkshop.App.csproj.nuget.dgspec.json
│       ├── EFCoreWorkshop.App.csproj.nuget.g.props
│       ├── EFCoreWorkshop.App.csproj.nuget.g.targets
│       ├── project.assets.json
│       └── project.nuget.cache
├── Dal
│   ├── Class1.cs
│   ├── EFCoreWorkshop.Dal.csproj
│   └── obj
│       ├── EFCoreWorkshop.Dal.csproj.nuget.dgspec.json
│       ├── EFCoreWorkshop.Dal.csproj.nuget.g.props
│       ├── EFCoreWorkshop.Dal.csproj.nuget.g.targets
│       ├── project.assets.json
│       └── project.nuget.cache
└── EFCoreWorkshop.sln
```

Teraz definicja wszystkich zainstalowanych lokalnych narzędzi będzie przechowywana w pliku `dotnet-tools.json`. Zachęcam do zapoznania sie z dokumentacją tej funkcjonalności na stronie [Microsoft](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-tool-install)

## Ćwiczenie 3 - Definiowanie modelu danych

Na tych warsztatch oprócz samego ORM-a Entity Framework Core będziesz także potrzebować model danych na którym można pracować. Aby lepiej osadzić te warsztaty w prawdziwych scenariuszach biznesowych przyjmijmy że chcemy zbudować system obsługujący jakąś klinikę weterynaryjną. Ta klinika specjalizuje się w leczeniu psów
a inne zwierzęta odsyła do jakiejś spółki córki. Dlatego w naszym procesie biznesowym będziemy zainteresowani tylko psami.

Nasz model danych może wyglądać następująco
```mermaid
classDiagram
class Owner {
  +Int Id
  +String FirstName
  +String LastName
  +Dog[] Dogs
  +String Note
}
class Dog {
  +DogNumber
  +Name
  +Owner Owner
  +int OwnerId
}
Owner <|-- Dog
```
Na nasze potrzeby ten dość uproszczony model powinień bez problemów wystarczyć.

Teraz przejdźmy do stworzenia bazy danych, możesz to zrobić za pomocą UI znajdujacego się w Sql Server Managment Studio. Tutorial jak to zrobić masz bardzo dobrze opisany w [tym miejscu](https://docs.microsoft.com/en-us/sql/relational-databases/databases/create-a-database?view=sql-server-ver15)

Ewentualnie jeżeli chcesz minimalnie poćwiczyć składnie SQL możesz utworzyć bazę danych za pomocą polecenia CREATE DATABASE. Poniżej znajdziesz kod, który utworzy bazę danych, jednak najpierw spróbuj sam napisać to zapytanie. 

::: details Kod SQL
```sql
USE master;
GO

IF DB_ID(N'EFCoreBasicWorkshop') IS NOT NULL
    DROP DATABASE EFCoreBasicWorkshop;
GO
CREATE DATABASE EFCoreBasicWorkshop;
GO
```
:::

### Database First

Kiedy już baza danych jest gotowa abyś zaczął na niej pracować to możemy przejść do pierwszego ćwiczenia z wykorzystaniem Entity Framework Core. Wykorzystamy podejście DataBase First aby "odtworzyć" model pod postacią klas C#. Aby to zrobić musimy najpierw stworzyć strukturę bazy danych. 
Spróbuj napisać definicję w języku SQL, która utworzy strukturę 

```mermaid
erDiagram
OWNER ||--o{ DOG : contains
OWNER {
  int Id
  string FirstName
  string LastName
  string Note
}
DOG {
  int DogNumber
  string Name
  int OwnerId
}
```

::: warning
Pamiętaj o utworzeniu klucza obcego pomiędzy tabelami Dogs i Owners. Jeżeli nie pamietasz jak to się robiło w czystym jezyku SQL to poniżej masz podpowiedź

```sql
-- w momencie tworzenia tabeli
CREATE TABLE X(
   SomeId int FOREIGN KEY REFERENCES Y(Id)
)

-- po utworzeniu tabeli
ALTER TABLE X
ADD FOREIGN KEY (SomeId) REFERENCES Y(Id)
```
:::

W pierwszej koleności spróbuj zrobić to samodzielnie, natomiast jeżeli będziesz miał problemy to poniżej znajdziesz rozwiązanie: 

::: details KOD SQL do tworzenia tabel
```sql
USE EFCoreBasicWorkshop;
GO;

CREATE TABLE Owners (
    Id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    FirstName nvarchar(max),
    LastName nvarchar(max),
    Note nvarchar(max)
)

CREATE TABLE Dogs (
    DogNumber int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    Name nvarchar(max),
    OwnerId int FOREIGN KEY REFERENCES Owners(Id)
)
```
:::

Następnie możesz wrócić do konsoli. Do kolejnej sekcji będziesz potrzebować wykorzystać "ConnectionString" do bazy danych. Jest to ciąg znaków, który definiuje jak aplikacja ma się połączyć z bazą danych. Poniżej w tabeli napisałem kilka najpopularniejszych, jednak jeżeli jesteś ciekawy to możesz zajrzeć na stronę [connectionstrings.com/sql-server](https://www.connectionstrings.com/sql-server/) aby zobaczyć jak różne biblioteki budują i wykorzystują connection string.

W poniższej tabeli wykorzystwywane są następujące zmienne: 
- `${Database}` - Nazwa bazy danych, z którą chcesz się połączyć
- `${User}` - Nazwa użytkownika używanego do uwierzytelnienia
- `${Password}` - Hasło użytkownika używanego do uwierzytelnienia

| Rodzaj Sql Server-a | Connection String |
| ---- | ---- |
| Sql Server on Container | Server=localhost,1433;Database=${Database};User Id=${User};Password=${Password} |
| LocalDb Visual Studio 2019 | Server=(localdb)\mssqllocaldb;Database=${Database};Trusted_Connection=True |

Następnie w konsolu przejdź do projektu App i uruchom polecenie

```bash
dotnet ef dbcontext scaffold "${ConnectionString}" Microsoft.EntityFrameworkCore.SqlServer --output-dir Models
```

Po wywołaniu tej komendy możesz zobaczyć że w projekcie pojawiły się nowe pliki

```{3-6}
.
├── EFCoreWorkshop.App.csproj
├── Models
│   ├── Dog.cs
│   ├── EfCoreBasicWorkshopContext.cs
│   └── Owner.cs
├── Program.cs
```

Tak wygląda model danych wygenerowany na podstawie inżynierii odwrotnej z bazy danych. 

::: tip
Poświęć chwilę na przejrzenie zawartości plików. Zobacz w jaki sposób są utworzone klasy. Sprawdź ile rzeczy dzieje się w klasie `EFCoreBasicWorkshopContext`
:::

## Ćwiczenie 4 - Code First

W ostatnim ćwiczeniu przeszliśmy przez definiowanie połączenia z bazą danych za pomoca podejścia "Database First". Jednak to podejście "Code First" jest zdecydowanie najpopularniejszym podejściem. 

Na początek utwórz klasę do przechowania konfiguracji. Nazwij ją `Settings`. Niech ta klasa będzie klasą statyczną i będzie zawierała tylko jeden propertis `ConnectionString` typu string. Możesz od razu ustawić wartość tego propertis-a na connection string potrzebny do połączenia się z Twoją instancją bazy danych. Przykładowa implementacja wygląda następująco. 

```csharp
public static class Settings
{
    public static readonly string ConnectionString = "Server=localhost,1433;Database=${Database};User Id=${User};Password=${Password}";
}
```

::: tip
Pamiętaj że connection string będzie wyglądał inaczej w zależności od tego jaki rodzaj bazy danych postawiłeś lokalnie
:::

W projekcie Dal utwórz model danych za pomocą kodu C#. Nie wzoruj się na kodzie wygenerowanym przez EF Core w ostatnim przykładzie, tylko zrób tak jak uważasz. Najlepiej jak najprościej.

Poniżej jak zawsze znajdziesz gotowe rozwiązanie, jednak zachęcam Cię abyś najpierw napisał kod samodzielnie a potem sprawdził jakie rozwiązanie jest poniżej.

::: details Rozwiązanie
Poniższe podejście jest najprostszym rozwiązaniem. Wykorzystuje obiekty POCO(Plain old C# objects).

```csharp
// Owner.cs
public class Owner
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Notes { get; set; }
    public ICollection<Dog> Dogs { get; set; }
}

// Dog.cs
public class Dog
{
    public int DogNumber { get; set; }
    public string Name { get; set; }
    public int OwnerId { get; set; }
}

```
:::

Następnie w tym samym katalogu utwórz klasę `MyContext`, która będzie dziedziczyć po klasie `DbContext`. Od razu przejdź do nadpisania metody `OnConfiguring`, w której skonfigurujemy jak EF Core ma się połączyć z bazą danych.

W metodzie `OnConfiguring` na obiekcie `DbContextOptionsBuilder` wywołaj następujące metody: 
- UseSqlServer
- EnableSensitiveDataLogging
- UseLoggerFactory

Do metody UseLoggerFactory będziesz musiał przekazać obiekt implementujący interfejs ILoggerFactory. Możesz zdefiniować taki obiekt na poziomie propertisu klasy za pomocą poniższego kodu: 

```csharp
static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });

```
Dzieki temu Entity Frameowrk Core będzie wypisywać na konsolę wszystkie szczegóły odnośnie zapytań do bazy danych, wraz z wygenerowanym kodem SQL.

Ponizej znajdziesz jak powinna wyglądać metoda OnConfiguring po Twoich zmianach.

::: details Rozwiązanie
```csharp
protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
{
    base.OnConfiguring(optionsBuilder);

    optionsBuilder.UseSqlServer(Settings.ConnectionString);
    optionsBuilder.EnableSensitiveDataLogging();
    optionsBuilder.UseLoggerFactory(MyLoggerFactory);
}
```
:::

Kolejnym elementem jest zdefiniowanie propertis-ów klasy `MyContext`. Musimy tutaj zdefiniować dwa propertisy, które zdefiniują tabele Owners i Dogs. 
W tych propertisach musimy wykorzystać generyczną klasę `DbSet<T>` gdzie T jest jedną z naszych encji. 
Spróbuj na podstawie prezentacji zdefiniować potrzebne DbSet-y.

::: details Rozwiązanie
```csharp
public DbSet<Dog> Dogs { get; set; }
public DbSet<Owner> Owners { get; set; }
```
::: 

Na koniec przejdz do projektu App i otwórz plik Program.cs. W metodzie main, napisz kod odpowiedzialny za wygenerowanie bazy danych. Jako że struktura danych będzie się zmieniać dość często to przed wywołaniem metody `EnsureCreated` możesz wywołać metodę `EnsureDeleted`. Tak aby baza danych była za każdym razem odtwarzana od nowa.

::: details Rozwiązanie
```csharp
using var context = new MyContext();

context.Database.EnsureDeleted();
context.Database.EnsureCreated();
```
:::

Jeżeli wszystko wykonałeś poprawnie to będziesz w stanie zobaczyć w konsoli błąd: 
```
Unhandled exception. System.InvalidOperationException: The entity type 'Dog' requires a primary key to be defined. If you intended to use a keyless entity type, call 'HasNoKey' in 'OnModelCreating'. For more information on keyless entity types, see https://go.microsoft.com/fwlink/?linkid=2141943.
```

Dzieje się tak ponieważ EF Core, nie jest w stanie określić, które pole encji Dog jest jej kluczem głównym. EF Core określa encję główną na trzy sposoby: 
- Za pomocą konwencji że każdy propertis o nazwie `Id` staje się kluczem głównym
- Za pomocą atrybutu `[Key]`
- Za pomocą fluent Api

Aby rozwiązać ten problem przejdź do pliku Dog.cs i dodaj przy propertis-ie `DogNumber` atrybut `[Key]`

Teraz już wszystko powinno zadziałać bez problemów a w konsoli aplikacji powinny pojawić się logi wygenerowane przez Entity Framework Core. 

```{29-35,38-44}
info: Microsoft.EntityFrameworkCore.Infrastructure[10403]
      Entity Framework Core 5.0.9 initialized 'MyContext' using provider 'Microsoft.EntityFrameworkCore.SqlServer' with options: SensitiveDataLoggingEnabled 
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (21ms) [Parameters=[], CommandType='Text', CommandTimeout='30']
      SELECT 1
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (31ms) [Parameters=[], CommandType='Text', CommandTimeout='60']
      IF SERVERPROPERTY('EngineEdition') <> 5
      BEGIN
          ALTER DATABASE [EfCoreBasicWorkshop] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
      END;
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (68ms) [Parameters=[], CommandType='Text', CommandTimeout='60']
      DROP DATABASE [EfCoreBasicWorkshop];
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (593ms) [Parameters=[], CommandType='Text', CommandTimeout='60']
      CREATE DATABASE [EfCoreBasicWorkshop];
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (212ms) [Parameters=[], CommandType='Text', CommandTimeout='60']
      IF SERVERPROPERTY('EngineEdition') <> 5
      BEGIN
          ALTER DATABASE [EfCoreBasicWorkshop] SET READ_COMMITTED_SNAPSHOT ON;
      END;
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (2ms) [Parameters=[], CommandType='Text', CommandTimeout='30']
      SELECT 1
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (6ms) [Parameters=[], CommandType='Text', CommandTimeout='30']
      CREATE TABLE [Owners] (
          [Id] int NOT NULL IDENTITY,
          [FirstName] nvarchar(max) NULL,
          [LastName] nvarchar(max) NULL,
          [Notes] nvarchar(max) NULL,
          CONSTRAINT [PK_Owners] PRIMARY KEY ([Id])
      );
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (3ms) [Parameters=[], CommandType='Text', CommandTimeout='30']
      CREATE TABLE [Dogs] (
          [DogNumber] int NOT NULL IDENTITY,
          [Name] nvarchar(max) NULL,
          [OwnerId] int NOT NULL,
          CONSTRAINT [PK_Dogs] PRIMARY KEY ([DogNumber]),
          CONSTRAINT [FK_Dogs_Owners_OwnerId] FOREIGN KEY ([OwnerId]) REFERENCES [Owners] ([Id]) ON DELETE CASCADE
      );
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (3ms) [Parameters=[], CommandType='Text', CommandTimeout='30']
      CREATE INDEX [IX_Dogs_OwnerId] ON [Dogs] ([OwnerId]);

```

Zwróć proszę uwagę na linie 29-35,38-44. To własnie w nich możesz zobaczyć kod SQL wygenerowany przez EntityFrameowrk Core potrzebny do utworzenia tabel w bazie danych.

## Ćwiczenie 5 - Konfiguracja Encji - Atrybuty

Mając utworzony model danych, możesz przejść do konfiguracji encji za pomocą atrybut-ów (adnotacji). Do najpopularniejszych atrybutów należą

#### Przestrzeń System.ComponentModel.DAtaAnnotations.Schema

| Nazwa | Cel |
| --- | --- |
| Table | Konfiguruje nazwę tabeli i / lub scheme w której się ona znajdzie |
| Column | Nazwa Kolumny do, której propertis będzie mapowany |
| Foreign Key | Definiuje propertis, na podstawie, którego zostanie wygenerowany klucz obcy |
| DatabaseGenerated | Definiuje jak baza ma generować wartość dla danego propertisa |
| NotMapped | Dodajemy do propertis-a, którego nie chcemy mapować do bazy danych |

#### Przestrzeń System.ComponentModel.DataAnnotations
| Nazwa | Cel |
| --- | --- |
| Key | Definiuje jeden lub więcej propertis-ów jako klucz |
| Timestamp | Definiuje typ danych columny jako rowversion |
| Required | Definiuje że propertis jest wymagany do zapisania danych |
| MaxLength | Definiuje maksymalną dozwoloną liczbę znaków w podanym propertisie |
| StringLength | Definiuje dozwolony przedział liczby znaków w podanym propertisie |

#### Zadanie

Aby oszczędzić trochę miejsca w bazie danych wprowadźmy trochę ograniczeń w obecnie stworzonych encjach. Ograniczenia, które musimy wprowadzić to: 

w Encji Owner: 
- [ ] FirstName musi być wymagane
- [ ] LastName musi być wymagane
- [ ] FirstName ma mieć od 2 do 255 znaków
- [ ] LastName ma mieć od 2 do 255 znaków
- [ ] Notes ma mieć maksymalnie 10000 znaków

w Encji Dog: 
- [ ] Name musi być wymagane
- [ ] OwnerId musi być wymagany
- [ ] Name ma mieć od 2 do 255 znaków

Teraz jeśli uruchomisz aplikację to schemat bazy danych zostanie automatycznie odświerzony. Jeżeli wszystko poszło zgodnie z planem to powinieneś w swoim IDE do bazy danych zobaczyć następujący widok: 

![Database Schema](./assets/db-schema-attr.png)

Jeżeli skończyłeś przed wszystkimi to możesz wybróbować inne atrybuty aby zmienić: 
- Nazwę Tabeli
- Nazwę Kolumny
- Zdefiniować klucz obcy za pomocą atrybutów
Pamiętaj tylko aby swoje zmiany zakoomentować, ponieważ póżniejsze ćwiczenia będą opierać się na strukturze, która już jest stworzona.

Natomiast jeżeli miałeś problemy z napisaniem tego rozwiązania to poniżej wstawiam gotowe rozwiązanie: 

::: details Rozwiązanie
```csharp
// Owner.cs
public class Owner
{
    public int Id { get; set; }
    [Required]
    [MaxLength(255)]
    [MinLength(2)]
    public string FirstName { get; set; }
    [Required]
    [MaxLength(255)]
    [MinLength(2)]
    public string LastName { get; set; }
    [MaxLength(10_000)]
    public string Notes { get; set; }
    public ICollection<Dog> Dogs { get; set; }
}
// Dog.cs
public class Dog
{
    [Key]
    public int DogNumber { get; set; }
    [Required]
    [MaxLength(255)]
    [MinLength(2)]
    public string Name { get; set; }
    [Required]
    public int OwnerId { get; set; }
}
```
::: 

## Ćwiczenie 6 - Konfiguracja Encji - Fluent Api

Atrybuty, chociaż szybkie w napisaniu są dość ograniczonym narzędziem. Zdecydowanie większe możliwości ma tzw. Fluent API, które pozwala np. na ustawienie precyzji w kolumnach posiadających wartości zmiennoprzecinkowe. 
W tym ćwiczeniu spróbujemy przenieść wszystkie konfigurację do wnętrza klasy `MyContext`

W tym celu musisz nadpisać metodę `OnModelCreating`, która jako swój parametr przyjmuje obiekt klasy `ModelBuilder`.

::: tip
Zwróć uwagę na to że annotacja dotycząca minimalnej liczby znaków nie działa ponieważ EF Core nie jest w stanie dodać takiego constrain-a. 

Jak ten problem rozwiązać na poziomie bazy dowiesz się w kolejnych ćwiczeniach, kiedy będziemy pisać własny kod SQL.
:::

Poniżej znajdziesz najprostsze rozwiązanie tego zadania: 

::: details Rozwiązanie
```csharp
protected override void OnModelCreating(ModelBuilder modelBuilder)
{
    base.OnModelCreating(modelBuilder);

    modelBuilder.Entity<Dog>()
        .HasKey(x => x.DogNumber);
    modelBuilder.Entity<Dog>()
        .Property(x => x.Name)
        .IsRequired()
        .HasMaxLength(255);
    modelBuilder.Entity<Dog>()
        .Property(x => x.OwnerId)
        .IsRequired();

    modelBuilder.Entity<Owner>()
        .Property(x => x.FirstName)
        .IsRequired()
        .HasMaxLength(255);

    modelBuilder.Entity<Owner>()
        .Property(x => x.LastName)
        .IsRequired()
        .HasMaxLength(255);

    modelBuilder.Entity<Owner>()
        .Property(x => x.Notes)
        .HasMaxLength(10_000);
}
```
:::

Jak widać ten kod nie będzie wyglądał najlepiej w momencie kiedy nasza aplikacja będzie zawierać 10, 50, 100 encji. Dobrą praktyką jest tworzenie funkcji, które nie ciągną się przez 200 / 500 linii. Dlatego też przejdźmy do kolejnego etapu warsztatów gdzie dowiesz się jak wydzielić konfigurację Encji do osobnego pliku.

## Ćwiczenie 7 - Konfiguracja Encji - IEntityTypeConfiguration

Wydzielenie konfiguracji encji służy do separacji warstwy infrastrukturalnej od warstwy domenowej (logiki biznesowej). W dużych projektach gdzie procesy mogą być dość skomplikowane taka separacja może przynieść wiele pozytywów (np. możliwość wymiany ORM, wymiany bazy danych na taką, która lepiej będzie się nadawać do problemów występujących w procesie biznesowym). 

W tym ćwiczeniu musimy wydzielić konfigurację encji Owner i Dog do osobnych plików konfiguracyjnych, wykorzystujących interfejs `IEntityTypeConfiguration`.

- [ ] Utwórz pliki `DogEntityTypeConfiguration.cs` i `OwnerEntityTypeConfiguration.cs` w projekcie Dal
- [ ] W pliku `DogEntityTypeConfiguration.cs` stwórz klasę o tej samej nazwie, implementującom interfejs `IEntityTypeConfiguration`
- [ ] Przenieś konfigurację encji Dog do wnętrza metody `Configure(EntityTypeBuilder<Dog> builder)`
- [ ] W pliku `OwnerEntityTypeConfiguration.cs` stwórz klasę o tej samej nazwie, implementującom interfejs `IEntityTypeConfiguration`
- [ ] Przenieś konfigurację encji Owner do wnętrza metody `Configure(EntityTypeBuilder<Owner> builder)`
- [ ] W pliku `MyContext.cs` w metodzie `OnModelCreating` usuń wszystkie wywołania modelBuilder-a dla encji `Dog` i `Owner`
- [ ] W pliku `MyContext.cs` w metodzie `OnModelCreating` zaimplementuj rejestrację wszystkich klas implementujących interfejs IEntityTypeConfiguration znajdujących się w obecnym projekcie
- [ ] Do klasy `MyContext.cs` dodaj propertis `Schema`, który będzie zwracał string "MyVet"
::: tip
Możesz to zrobić za pomocą poniższego kodu: 

```csharp
public static string Schema => "MyVet";
```
:::
- [ ] Wykorzystując statyczny propertis `MyContext.Schema` dodaj do konfiguracji obu encji kod odpowiedzialny za utworzenie tych tabel w schemie "MyVet"
::: tip
Aby nie hardkodować nazw tabel możesz użyć operatora `nameof`

```csharp
string name = nameof(MyContext.Owners);
```
:::

Jeżeli utknąłeś na jakimś punkcie to poniżej znajdziesz rozwiązania: 

::: details DogEntityTypeConfiguration
```csharp
public class DogEntityTypeConfiguration : IEntityTypeConfiguration<Dog>
{
    public void Configure(EntityTypeBuilder<Dog> builder)
    {
        builder.HasKey(x => x.DogNumber);
        builder.Property(x => x.Name)
            .IsRequired()
            .HasMaxLength(255);
        builder.Property(x => x.OwnerId)
            .IsRequired();
            
        builder.ToTable(nameof(MyContext.Dogs), MyContext.Schema);
    }
}
```
:::

::: details OwnerEntityTypeConfiguration
```csharp
public class OwnerEntityTypeConfiguration : IEntityTypeConfiguration<Owner>
{
    public void Configure(EntityTypeBuilder<Owner> builder)
    {
        builder.Property(x => x.FirstName)
            .IsRequired()
            .HasMaxLength(255);

        builder.Property(x => x.LastName)
            .IsRequired()
            .HasMaxLength(255);

        builder.Property(x => x.Notes)
            .HasMaxLength(10_000);
            
        builder.ToTable(nameof(MyContext.Owners), MyContext.Schema);
    }
}
```
:::

::: details OnModelCreating
```csharp
protected override void OnModelCreating(ModelBuilder modelBuilder)
{
    base.OnModelCreating(modelBuilder);

    modelBuilder.ApplyConfigurationsFromAssembly(typeof(MyContext).Assembly);
}
```
:::

## Ćwiczenie 8 - Wrap It Up!

Przerobiliśmy już całkiem sporo możliwości konfiguracyjnych Entity Framework Core. Dlatego teraz pora utrwalić trochę wiedzę. Wykonaj następującą listę akcji aby jeszcze raz przećwiczyć to co omawialiśmy.

Wykorzystując swoją wiedzę z poprzednich ćwiczeń: 

- [ ] Utwórz encję `Doctor` z następującymi propertisami
```mermaid
classDiagram
class Doctor {
    +int Id 
    +String FirstName
    +String LastName
    +DateTime PracticeFrom
    +int YearsOfPractice
}
```
Propertis `YearsOfPractice` powinien być wyliczany na podstawie propertisa `PracticeFrom`

::: tip
Aby wyliczyć różnicę lat możesz użyć poniższej funkcji. Nie jest ona całkowicie dokładna ale na potrzeby warsztatu będzie wystarczająca.

```csharp
(int)((DateTime.Today - PracticeFrom).Days / 365.25m);
```
:::

- [ ] Utwórz encję `Visit` z następującymi propertisami
```mermaid
classDiagram
class Visit {
    +int Id
    +int DoctorId
    +int? DogNumber
    +int OwnerId
    +DateTime StartsAt
    +DateTime EndsAt
}
```
- [ ] Utwórz klasę `DoctorEntityTypeConfiguration` implementującą interfejs `IEntityTypeConfiguration`
- [ ] Napisz konfigurację klasy `Doctor` tak aby spełnić poniższe kryteria
    - [ ] Id powinno być kluczem głównym
    - [ ] FirstName ma być wymagane
    - [ ] FirstName ma mieć maksymalnie 255 znaki
    - [ ] LastName ma być wymagane
    - [ ] LastName ma mieć maksymalnie 255 znaki
    - [ ] YearsOfPractice ma być ignorowane
    - [ ] PracticeFrom ma być wymagane
    - [ ] PracticeFrom ma zawierać prezycję 1
    - [ ] Tabela powinna mieć nazwę `Doctors`
    - [ ] Tabela powinna znajdować się w Scheme `MyVet`
- [ ] Napisz konfigurację klasy `Visit` tak aby spełnić poniższe kryteria
    - [ ] Id powinno być kluczem głównym
    - [ ] DoctorId ma być wymagane
    - [ ] OwnerId ma być wymagane
    - [ ] DogNumber ma być opcjonalny
    - [ ] StartsAt ma być wymagane
    - [ ] StartsAt ma zawierać precyzję 1
    - [ ] EndsAt ma być wymagane
    - [ ] EndsAt ma zawierać precyzję 1
    - [ ] Tabela powinna mieć nazwę `Visits`
    - [ ] Tabela powinna znajdować się w Scheme `MyVet`
- [ ] Dodaj DbSet-y w klasie `MyContext`

Oczekiwanym efektem końcowym powinna być następująca struktura bazy danych: 

![Database Structure](./assets/db-schema-cw-8.png)

## Ćwiczenie 9 - Zapis danych do bazy danych

#### Wykonywane z wykładowcą

- [ ] Przejdź do pliku `Program.cs`
- [ ] Poniżej wywołań `EnsureCreated` i `EnsureDeleted` utwórz obiekt Owner-a o poniższych właściwościach: 

| Nazwa | Wartość |
| --- | --- |
| FirstName | Twoje Imie |
| LastName | Twoje Nazwisko |
| Notes | |

- [ ] Następnie dodaj utworzoną encję do context-u Entity Framework Core
- [ ] Wywołaj metodę `SaveChanges` na obiekcie kontekstu
- [ ] Uruchom aplikację, uwaznie obserwując logi tak aby zobaczyć jakie zapytania SQL generuje EntityFramework Core
- [ ] Sprawdź w bazie danych czy dodany obiekt zapisał się poprawnie

::: details Rozwiązanie
```csharp
var owner = new Owner
{
    FirstName = "Kamil",
    LastName = "Kielbasa",
    Notes = string.Empty
};

context.Owners.Add(owner);
context.SaveChanges();
```
:::

#### Wykonywane samodzielnie

- [ ] W pliku `Program.cs` zdefiniuj jeszcze jedną instancje klasy Owner 
- [ ] W pliku `Program.cs` zdefiniuj jeszce trzy instancje klasy Dog
- [ ] Do pierwszej instancji klasy Owner dodaj jedną instancję klasy Dog
- [ ] Do drugiej instancji klasy Owner dodaj pozostałe dwie instancje klasy Dog

::: tip
Jako że w obecnej implementacji klasy Owner list obiektów klasy Dog nie jest zainicjalizowana to możesz: 

- Dodać bezparametrowy konstruktor, który inicjalizuje listę:
```csharp
public Owner() {
    Dogs = new List<Dog>();
}
```
- Zainicjalizować listę w propertisie
```csharp
public ICollection<Dog> Dogs = new List<Dog>();
```
- Wstawić listę podczas tworzenia obiektu
```csharp
var owner = new Owner {
    Dogs = new List<Dog> { new Dog() }
}
```
:::

- [ ] Uruchom aplikację aby sprawdzić czy wszystko zapisało sie poprawnie
- [ ] Otwórz swojego klienta bazy danych aby zobaczyc jak tabele Owners i Dogs zostały dostały po kilka rekordów
- [ ] Przedyskutuj swoje rozwiązanie z wykładowcą

::: warning
Pamiętaj że na chwilę obecną za każdym razem kiedy uruchamiasz aplikację cała baza danych jest odtwarzana, co oznacza że po każdym uruchomieniu aplikacji w bazie znajdują się nowe rekordy
:::

Baza danych po uruchomieniu aplikacji powinna wyglądać następująco: 

__Tabela Owners:__

| Id | FirstName | LastName | Notes |
| --- | --- | --- | --- |
| 1 | Kamil | Kiełbasa | |
| 2 | Jan | Kowalski | |

__Tabela Dogs:__

| DogNumber | Name | OwnerId |
| --- | --- | --- |
| 1 | Burek | 1 |
| 2 | Szarik | 2 |
| 3 | Lessie | 2 |

::: details Przykładowe Rozwiązanie
```csharp
var owner = new Owner
{
    FirstName = "Kamil",
    LastName = "Kielbasa",
    Notes = string.Empty,
    Dogs = new List<Dog> { new (){ Name = "Burek" } }
};

var secondOwner = new Owner
{
    FirstName = "Jan",
    LastName = "Kowalski",
    Dogs = new List<Dog> { new (){ Name = "Szarik" }, new (){ Name = "Lessie"} }
};

context.Owners.Add(owner);
context.Owners.Add(secondOwner);
context.SaveChanges();
```
:::

## Ćwiczenie 10 - Odczyt danych z bazy danych

Masz już za sobą zapis danych do bazy danych. To teraz przydałoby się odczytać te dane.

- [ ] Pobierz wszystkie zapisane w bazie instancje klasy `Owner`
- [ ] Pobierz wszystkie zapisane w bazie instancje klasy `Dog`
- [ ] Przy pobieraniu wszystkich instancji klasy Owner rozkaz EF Core aby do każdego Owner załadował także instancje klasy `Dog`
- [ ] Uruchom aplikację w trybie Debug i zatrzymaj jej wykonywanie zaraz za wywołaniem metody `ToList`, zobacz jak wyglądają obiekty odtworzone przez EF core na podstawie danych z bazy danych
- [ ] Do klasy `Dog` dodaj navigation property Owner: Owner
- [ ] Ponownie uruchom aplikację aby zobaczyć jak teraz bedzie wyglądał obiekt klasy `Dog`
- [ ] Napisz wywołanie LINQ, za pomocą którego pobierzesz tylko pierwszy element listy

::: details Rozwiązanie
```csharp
var owners = context.Owners
    .Include(x => x.Dogs)
    .Take(1)
    .ToList()
    .Single();
```
:::

## Ćwiczenie 11 - Edycja danych

- [ ] Wykorzystując kod z ćwiczenia 11 zmodyfikuj obiekt owner-a
- [ ] Następnie ponownie wywołaj metodę `context.SaveChanges` 
- [ ] Uruchom aplikację w trybie Debug i obserwuj jakie zapytanie do bazy danych zostanie wygenerowane przez EF Core
- [ ] Zmodyfikuj instancję obiektu Dog znajdującego się wewnątrz obiektu Owner
::: tip
Najlepiej to zrobić przed wywołaniem `context.SaveChanges` inaczej EF Core nie uaktualni bazy danych. 
Ewentualnie innym rozwiązaniem jest ponowne wywowałnie metody `context.SaveChanges()`
:::
- [ ] Uruchom aplikację w trybie Debug i obserwuj jakie zapytanie do bazy danych zostanie wygenerowane przez EF Core
- [ ] Wykorzystując metodę `Find` pobierz z bazy danych tylko obiekt Owner-a o Id 2, dodając do niego również kolekcję obiektów `Dog`

::: details Rozwiązanie
```csharp
owner.FirstName = "Test";

owner.Dogs.First().Name = "Dogggo";

context.SaveChanges();
```
:::

## Ćwiczenie 12 - Usuwanie danych

- [ ] Wykorzystując kod z Ćwiczenia 11 usuń obiekt owner-a o id 1 z bazy danych za pomocą metody `Remove`

::: danger
Jako że wszystkie obiekty są załadowane do pamięci i EntityFrameowrk Core posaida do nich referencje, nie próbuj usuwać elementów przez "skrócony zapis": 

```csharp
context.Owners.Remove(new Owner { Id = 1 });
```

Ponieważ taki zapis rzuci wyjątkiem, że Entity Frameowrk Core ma taki obiekt już załadowany do swojej pamięci i nasłuchuje na wszystkie jego zmiany.
:::

::: details Rozwiązanie
```csharp
context.Owners.Remove(owner);

context.SaveChanges();
```
:::