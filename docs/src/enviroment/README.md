# Przygotowanie Środowiska
Witaj na warsztatach Entity Framework Core poziom podstawowy. 

W tej sekcji dowiesz się jak skonfigurować swoje środowisko developerskie aby przejść przez te warsztaty jeszcze raz w domu.

## Oprogramowanie
Przyda się na pewno:
- IDE (jedno z):
  - [Visual Studio Community Edition](https://visualstudio.microsoft.com/pl/downloads/)
  - [Rider](https://www.jetbrains.com/rider/) (Narzędzie płatne) <Badge text="Wybór prowadzącego"/>
  - [Visual Studio Code](https://code.visualstudio.com/download)
- IDE dla bazy danych (jedno z):
  - [Sql Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)
  - [DataGrip](https://www.jetbrains.com/datagrip/) (Narzędzie płatne) <Badge text="Wybór prowadzącego"/>
  - [SqlElectro](https://sqlectron.github.io/)
- [Postman](https://www.getpostman.com/)
- Przeglądarka internetowa (Chrome / Firefox)
- Baza Danych Sql Server (jedna z):
  - [SqlServer LocalDb](https://docs.microsoft.com/en-us/sql/database-engine/configure-windows/sql-server-express-localdb?view=sql-server-ver15) powinna być preinstalowana wraz z Visual Studio
  - [SqlServer Express](https://www.microsoft.com/pl-pl/sql-server/sql-server-downloads)
  - [SqlServer on Container](https://hub.docker.com/_/microsoft-mssql-server) <Badge text="Wybór prowadzącego"/>
- [Git](https://git-scm.com/)
- Terminal
  - Git Bash - Jest instalowany razem z Git-em
  - [Cmder](https://cmder.net/) - trochę bardziej zaawansowana powłoka dla systemu operacyjnego windows
  - [ITerm2](https://iterm2.com/) - trochę bardziej zaawansowany terminal dla systemu Mac Os X
## Postman
Podczas warsztatów bedą wykonywane szyfrowane połaczenia lokalne (https i wss). Koniecznym jest skonfigurowanie Postman-a, aby nie walidował certyfikatów SSL. Wystarczy wejsć w ustawienia Postman-a i odznacz zaznaczoną na poniższym obrazku opcję.
![An image](https://linecode.pl/workshops/assets/postman.png)
## .NET
Podstawą do uczestnictwa w warsztatach jest posiadanie zainstalowanej platformy .NET Core w wersji 5.0+ na swoim komputerze. Potrzebne SDK możecie znaleźć tutaj:
[https://dotnet.microsoft.com/download](https://dotnet.microsoft.com/download)
### Testowy Projekt
Po zainstalowaniu .NET SDK uruchom kilka komend w konsoli, aby sprawdzić czy wszystko działa poprawnie.
 
Zacznijmy od sprawdzenia dotnet CLI, polecenie:
```
dotnet –-version
```
Powinno wypisać na konsoli obecnie zainstalowaną wersję dotnet-a np. (5.0.100)
Następnie sprawdź, czy jesteś w stanie wygenerować nowy projekt wraz z solucją. W przypadku używania konsoli git bash lub systemu linux / mac os to gotowy skrypt masz poniżej. 
``` 
cd ./workspace
mkdir testProject
cd testProject
dotnet new sln
mkdir src
dotnet new mvc -n TestProject -o ./src/TestProject
dotnet sln add **/*csproj
dotnet restore
dotnet build
cd ./src/TestProject
dotnet run
```
Jeżeli jesteś użytkownikiem Windows-a polecam wygenerowanie projektu i uruchomienie go za pomocą Visual Studio.
 
### Instalcja certyfikatów SSL
Instalacja wymaga posiadania certyfikatów SSL na swoim urządzeniu.  Wykorzystaj dotnet CLI.
```
dotnet dev-certs https --trust
```
Po wykonaniu tej komendy zostaniesz poproszony o potwierdzenie, czy na pewno chcesz dodać certyfikat. Ponieważ systemy operacyjne nie będą mogły zweryfikować poprawności certyfikatu dla domeny „localhost”, po zaakceptowaniu komunikatu i zrestartowaniu aplikacji, powinna się ona odpalić już pod bezpiecznym połączeniem po protokole https.

![An Image](https://linecode.pl/workshops/assets/ssl.png)
## Baza Danych
Do przejścia tych warsztatów będziesz potrzebować bazy danych SqlServer 2017+. 
Moze to być localDB zainstalowana razem z Visual Studio.

Innym sposobem na postawienie lokalnej bazy danych jest uruchomienie oficjalnego kontenera. Instrukcje jak to zrobić mozna znaleźć [tutaj](https://bd90.pl/sqlserver-na-mac-os-x/)

Oprócz samej bazy danych przyda się tez jakieś IDE do bazy np.
- Sql Server Management Studio ([Do Ściągnięcia tutaj](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15))
- JetBrains Data Grip

Pamiętaj aby przed przejściem do kolejnej sekcji sprawdzić czy możesz połączyć się z bazą danych.
Tutaj masz tutoriale jak nawiązać połączenie za pomocą: 
- [Sql Server Managment Studio](https://docs.microsoft.com/en-us/sql/ssms/quickstarts/ssms-connect-query-sql-server?view=sql-server-ver15)
- [Data Grip](https://www.jetbrains.com/help/datagrip/db-tutorial-connecting-to-ms-sql-server.html)
