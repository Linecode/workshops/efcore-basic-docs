const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'EF Core - Poziom podstawowy',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,
  
  base: '/workshops/efcore-basic-docs/',
  dest: '../public',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    logo: 'https://linecode.pl/images/linecode-logo.png',
    nav: [
      {
        text: 'Środowisko',
        link: '/enviroment/'
      },
      {
        text: 'Dzień I',
        link: '/day-1/',
      },
      {
        text: 'Dzień II',
        link: '/day-2/'
      },
      {
        text: 'Dzień III',
        link: '/day-3/'
      },
      {
        text: 'Addons',
        link: '/extra/'
      }
    ],
    sidebar: "auto"
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    'vuepress-plugin-mermaidjs'
  ],
  
  markdown: {
    extendMarkdown: md => {
      md.use(require('markdown-it-task-lists'), {enabled: true})
    }
  }
}
