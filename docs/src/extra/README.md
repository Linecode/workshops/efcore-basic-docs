---
home: true
footer: Copyright © 2021-present Kamil Kiełbasa - Linecode
---

## Mała reklama:

- Mój blog: [bd90.pl](https://bd90.pl)
- Mój profil na LN: [bd-90](https://www.linkedin.com/in/bd-90/)
- Mój profil na Twitter: [_bd_90](https://twitter.com/_bd_90)

## Polecane Książki: 

- [Entity Framework Core In Action](https://www.manning.com/books/entity-framework-core-in-action-second-edition)
- [Patterns of Enterprise Application ARchitecture](https://www.amazon.com/Patterns-Enterprise-Application-Architecture-Martin/dp/0321127420)
- [Domain Driven Design: Tackling Complexity int hte Heart of Software](https://www.amazon.pl/Domain-Driven-Design-Tackling-Complexity-Software/dp/0321125215/)