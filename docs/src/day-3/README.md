# Dzień Trzeci

Witaj na drugim trzecim warsztatów. Dzisiaj będziemy przerabiali najbardziej zaawansowane rzeczy na tych warsztatach. Zaczniemy od change tracker-a, potem przejdziemy do obsługi dziedziczenia, migracji i zakończymy dzień pisaniem asynchronicznego kodu.

## Ćwiczenie 1 - Change Tracker

Change Tracker jest to wbudowane narzędzie w EntityFramework, które służy do nasłuchiwania na zmiany obiektów podpiętych do changeTacker-a (wszystkie, których stan nie jest ustawiony na `detached`). Jak było poruszone na części teoretycznej uruchamianie tego mechanizmu jest w pełni zarzadzalne. 

Zależność pomiędzy zmianą stanu i różnymi parametrami encji, bardzo ładnie obrazuje poniższa tabela: 

| Entity State | Tracker By DbContext | Exists In Database | Properties Modified | Action On SaveChanges |
| --- | --- | --- | --- | --- |
| Detached | No | - | - | - | 
| Added | Yes | No | - | Insert |
| Unchaged | Yes | Yes | No | - |
| Modified | Yes | Yes | Yes | Update |
| Deleted | Yes | Yes | - | Delete |

Teraz wykonaj kilka operacji aby sprawdzić jak ten mechanizm działa. 

- [ ] Przejdź do pliku `MyContext.cs`
- [ ] Przed wywołaniem `base.SaveChanges` dodaj linijkę

```csharp
var changedEntities = ChangeTracker.Entries();
```

- [ ] Ustaw debugger po tej linijce
- [ ] Uruchom aplikację w trybie `Debug`
- [ ] Sprawdź jakie encję zostały złapane w ChangeTracker, zobacz jak wyglądają obiekty na których bazuje EF Core
- [ ] Następnie przejdź do repozytorium
- [ ] Do jakiejś metody pobierającej obiekty z bazy danych dodaj linijkę `AsNoTracking`
- [ ] Ponownie uruchom aplikację w trybie `Debug` i zobacz co się stanie

::: tip
Jeżeli chcesz przetestować tylko pobieranie infomacje wraz z budowaniem obiektu Proxy przez Entity Framework Core, to najlepiej po wcześniejszym uruchomieniu projektu zakomentować linijki kodu odpowiedzialnego za odtwarzania bazy danych z pliku `Program.cs`. 
Wtedy EF Core będzie musiał pobrać encję z bazy danych a nie bazować na encjach, które już ma załadowane do obecnego kontekstu
:::

Change tracker służy nie tylko do odczytywania wartości, w tym miejscu możemy nadpisać bardzo duże ilości zachowań EF Core. Dla przykładu: 

- [ ] Dodaj do jakiejś encji `ShadowProperty` `CreatedAt`
- [ ] Następnie przejdź do pliku `MyContext`
- [ ] W metodzie SaveChanges dodaj poniższy kod: 

```csharp
foreach (var item in ChangeTracker.Entries())
{
    if (item.State == EntityState.Added)
    {
        item.CurrentValues["CreatedAt"] = DateTimeOffset.Now;
    }
}
```

- [ ] Uruchom aplikację w trybie `Debug` i sprawdź jak się zachowuje pole `CurrentValues` w obiekcie `ChangeTracker`

## Ćwiczenie 2 - Debugowanie Change Tracker-a

Bugi / Dziwne zachowania zdarzają się niemal zawsze. Dlatego bardzo ważna jest umiejętność i znajomość narzędzi pozwalających na zrozumienie w jaki sposób można określić źródło problem-ów. 
Kiedy mam problemy z Entity Framework Core, zazwyczaj zaczynam debugowanie od sprawdzenia co się dzieje wewnątrz ChangeTracker-a. Można to zrobić w bardzo prosty sposób. 

- [ ] Przejdź do pliku `Program.cs`
- [ ] Następnie zaraz przed wywołanie metody `SaveChanges` dodaj następujący kod: 

```csharp
context.ChangeTracker.DetectChanges();
Console.WriteLine(context.ChangeTracker.DebugView.LongView);
```

::: warning
Pamiętaj aby dostosować nazwę zmiennej `context` do swojej nazwy DbContext-u w projekcie
:::

## Ćwiczenie 3 - Soft Delete 

W wielu systemach IT możemy się spotkać z mechanizmem `SoftDelete`. W dużym skrócie to nie usuwamy danych z bazy danych tylko dodajemy kolumnę `IsDeleted`, którą ustawiamy na `true`. 

Twoim zadaniem będzie stworzenie mechanizmu, który w przypadku próby usunięcia danego rekordu zmieni stan encji na modified i ustawi kolumnę IsDeleted na true.

Możesz w pierwszej kolejności spróbować napisać mechanizm własnymi siłami a dopiero potem posiłkować się poniższą listą kroków.

- [ ] W wszystkich encjach dodaj ShadowProperty o nazwie `IsDeleted`
- [ ] W wszystkich encjach dodaj ShadowProperty o nazwie `DeletedAt`
- [ ] Następnie przejdź do klasy `MyContext` i w metodzie `SaveChanges` dodaj sprawdzenie czy stan encji to `Deleted`
- [ ] Jeżeli tak 
    - [ ] To zmień stan tej encji na `Modified`
    - [ ] Ustaw wartość `IsDeleted` na true za pomocą słowanika `CurrentValues`
    - [ ] Ustaw wartość `DeletedAt` na `DateTime.UtcNow`

## Ćwiczenie 4 - Dziedziczenie (Table Per Hierarchy)

Kolejny dzień w pracy, kolejne nowe wymagania biznesowe. Tym razem analityk przyszedł do Twojego biura i opowiedział o nowym pomyśle. Właściciel kliniki chciałby aby system obsługiwał nie tylko psy ale także inne zwierzęta. Podobno jest dość duże zainteresowanie naszymi usługiami przez społeczność właścicieli królików, a przynajmniej tak twierdzą ludzie od marketingu. 

- [ ] W projekcie `Dal` zdefiniuj klasę abstrakcyjną `Animal`
- [ ] Niech `Animal` posiada następujące pola: 
    - [ ] `int Id`
    - [ ] `string Name`
    - [ ] `ICollection<Owner> Owner`
- [ ] Następnie zmodyfikuj klasę `Dog` tak aby dziedziczyła nie po `BaseEntity` tylko po klasie `Animal`
- [ ] Usuń niepotrzebne pola z klasy `Dog`
- [ ] Dostosuj plik `DogEntityTypeConfiguration`
- [ ] Dodaj klasę `Rabbit`
- [ ] Klasa `Rabbit` musi dziedziczyć po klasie `Animal`
- [ ] Do klasy `Rabbit` dodaj takie pola jak: 
    - [ ] `string Breed`
    - [ ] `int Weight`
- [ ] Utwórz plik `AnimalEntityTypeConfiguration`
- [ ] Skonfiguruj aby Discriminator był
    - [ ] typu int
    - [ ] Klasa Dog zapisywała się jako 1
    - [ ] Klasa Rabbit zapisywała się jako 2
- [ ] W klasie `MyContext` utwórz nowy `DbSet` zawierający encję królików
- [ ] Utwórz Repozytorium pozwalające na pobieranie i zapisywanie obiektów klasy `Dog` / `Rabbit`
- [ ] Uruchom aplikację i sprawdź czy wszystko działa poprawnie

::: tip <Badge text="Dla chętnych"/>
Istnieje możliwość osiągnięcia tego samego rezultatu dodając tylko jeden DbSet z klasą bazową: 

Zakładając poniższy model: 

```csharp
public class Contract
{
    public int ContractId { get; set; }
    public DateTime StartDate { get; set; }
    public int Months { get; set;}
    public decimal Charge { get; set; }
}
public class MobileContract : Contract
{
    public string MobileNumber { get; set; }
}
public class TvContract : Contract
{
    public PackageType PackageType { get; set; }
}
public class BroadBandContract : Contract
{
    public int DownloadSpeed { get; set; }
}
public enum PackageType
{
    S, M, L, XL
}
```

Implementacja, pozwalająca na korzystanie z pojedyńczego DbSet-a wyglądałaby następująco: 

```csharp
public class SampleContext : DbContext
{
    public DbSet<Contract> Contracts { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<MobileContract>();
        modelBuilder.Entity<TvContract>();
        modelBuilder.Entity<BroadBandContract>();
    }
}
```

Wtedy aby wczytać tylko encję danego typu trzeba wywołać metodę `OfType<T>`

```csharp
var tvContracts = context.Contracts.OfType<TvContract>().ToList();
```

- [ ] Spróbuj przekształcić implementację z ćwiczenia 2 na implementację z jednym DbSet-em
:::

## Ćwiczenie 5 - Dziedziczenie (Table Per Type Configuration)

- [ ] Dostosuj konfigurację z pliku `DogEntityTypeConfiguration`
- [ ] Dostosuj konfigurację z pliku `AnimalEntityTypeConfiguration`
- [ ] Utwórz plik `RabbitEntityTypeConfiguration`
- [ ] Uruchom aplikację i sprawdź czy wszystko działa poprawnie

## Ćwiczenie 6 - Migracje

Migracje są jednym z najważniejszych funkcjonalności EntityFramework-a. Pozwalają na deklaratywne zarządzanie schematem bazy danych. 

::: warning
Przed wykonaniem jakiegokolwiek z ćwiczeń pamiętaj o usunięciu lini: 

```csharp
context.Database.EnsureDelete();
context.Database.EnsureCreated();
```

z pliku `Program.cs`. Od tego momentu baza danych będzie tworzona / modyfikowana tylko kiedy będziemy tego chcieli. 
:::

#### Dodawanie

Migracje będziemy dodawać / uruchamiać z projektu App. Zacznijmy od wygenerowania pierwszej migracji: 

```bash
dotnet ef migrations add InitialMigration
```

Ta komenda powinna wygenerować pierwszą migrację. Jak pewnie zauważyłeś / zauważyłaś ten kod wygenerował pliki w projekcie `App`

```bash
➜ tree Migrations
Migrations
├── 20210908182840_InitialMigration.Designer.cs
├── 20210908182840_InitialMigration.cs
└── EfCoreBasicWorkshopContextModelSnapshot.cs
```

Każdą migrację możemy po prostu usunąć, jeżeli nie została ona wgrana do bazy danych. Dlatego najszybszym sposobme na pozbycie się tych plików będzie wywołanie komendy w terminalu: 

```
rm -rf Migrations
```

Aby migracje dodawały się w projekcie `Dal` to musimy dokonać jednej zmiany w pliku `MyContext`. Mianowicie w metodzie `OnConfiguring` trzeba zdefiniować do jakiej assembly chcemy migrować. 

```csharp
protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
{
    base.OnConfiguring(optionsBuilder);

    optionsBuilder.UseSqlServer(Settings.ConnectionString, x => x.MigrationsAssembly(typeof(MyContext).Assembly.FullName));
```

Następnie migrację trzeba generować z dodatkowymi parametrami: 

```bash
➜ dotnet ef migrations add InitialMigration --project ../Dal --context MyContext
```

Musimy podać, zarówno ścieżkę do projektu jak i klasę DbContext, którą chcemy zmigrować. Jeżeli mamy kilka klas DbContext w projekcie to na chwilę obecną musimy migrować je oddzielnie. 

Po wygenerowaniu migracji możesz przejść do uaktualnienia schematu bazy danych. Można to zrobić na trzy sposoby. Pierwszy to wykorzystanie dotnet CLI. 

```bash
➜ dotnet ef database update --project ../Dal --context MyContext
```

Drugim sposobem jest wygenerowanie skryptu SQL, który można ręcznie wgrać do bazy danbych

```bash
➜ dotnet ef migrations script --project ../Dal --context MyContext
```

Trzecim sposobem jest dodanie kodu odpowiedzialnego za automigrowanie bazy danych na starcie aplikacji: 

```csharp
using var context = new MyContext(); 
context.Database.Migrate();
```

Przed uruchomieniem funkcji Migrate zalecane jest sprawdzenie czy mamy jakieś oczekujące migracje

```csharp
if (context.Database.GetPendingMigrations().Any())
```

##### Wykorzystanie kodu SQL w migracjach

Api obiektu `migrationBuilder` daje nam możliwość dodawania własnego kodu SQL do uruchomienia podczas migracji. Mamy do do naszej dyspozycji kilka mechanizmów pomagających nam w tym celu. 

Pierwszym jest wykorzystanie `migrationBuilder.Sql`. Dla przykładu migracja, w której będziemy dodawać widok materializowany do bazy danych wygląda następująco: 

```csharp
public partial class AddTableView : Migration
{
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql($"CREATE VIEW {DbContext.DefaultSchema}.{ReadModelEntityTypeConfiguration.ViewName} WITH SCHEMABINDING AS " +
                                $"SELECT Id, Name from {DbContext.DefaultSchema}.Table;");

        migrationBuilder.Sql($"CREATE UNIQUE CLUSTERED INDEX IX_Table_Id ON {DbContext.DefaultSchema}.{ReadModelEntityTypeConfiguration.ViewName}(id);");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql($"DROP INDEX IX_Table_Id ON {DbContext.DefaultSchema}.{ReadModelEntityTypeConfiguration.ViewName}(id);");
        migrationBuilder.Sql($"DROP VIEW {DbContext.DefaultSchema}.{ReadModelEntityTypeConfiguration.ViewName};");
    }
}

```

W momencie kiedy wykonujemy dużo powtarzających się operacji możemy taka operację zamknąć w extension dla obiektu `migrationBuilder`

```csharp
private static OperationBuilder<SqlOperation> CreateUser(
    this MigrationBuilder migrationBuilder,
    string name,
    string password)
    => migrationBuilder.Sql($"CREATE USER {name} WITH PASSWORD '{password}';");
```

Nastepnie możemy używać tego extension jak każdej innej metody obiektu migration builder

```csharp
migrationBuilder.CreateUser("SQLUser1", "Password");
```

::: tip
Jeżeli mamy problem z uruchomieniem migracji ponieważ "wyrażenie" musi być pierwszym albo jedynym w batch-u SQL to można spróbować użyć funkcji `EXEC`

Dla przykładu EXEC przyda się w momencie kiedy będziemy potrzebowali stworzyć procedurę składowaną

```csharp
migrationBuilder.Sql(
@"
    EXEC ('CREATE PROCEDURE getFullName
        @LastName nvarchar(50),
        @FirstName nvarchar(50)
    AS
        RETURN @LastName + @FirstName;')");
```
:::

#### Kasowanie Migracji

Aby usunąć migrację wystarczy wywołać metodę CLI: 

```bash
➜ dotnet ef migrations remove
```

::: warning
Nie jest zalecane aby kasować migrację, które zostały już dodane do produkcyjnej bazy danych. Jeżeli to zrobimy istnieje prawdopodobieństwo że nie będziemy w stanie odwrócić tych zmian.
:::

#### Resetowanie migracji

W niektórych przypadkach może być potrzebne usunięcie wszystkich migracji i rozpoczecie wszystkiego od nowa. Może to być łatwe do osiągnięcia poprzez skasowanie folderu `Migrations` i zdropowanie bazy. Po tych akcjach możesz stworzyć nową migrację inicjalizującą. 

Istnieje także możliwość resetowanie migracji i utworzenia tylko jeden bez utracenia danych. Wtedy proces wygląda następująco: 

- [ ] Skasuj folder Migrations
- [ ] Utwórz nową migrację i wygeneruj skrypt SQL
- [ ] W bazie danych usuń wszystkie rekordy w tabeli z migracjami
- [ ] Utwórz w tabeli z migracjami jeden rekord, z wpisem z wcześniej wygenerowaną migracją. 

::: warning
Jeżeli wcześniejsze migracje zawierały jakąś customizację to będzie musiała być ona naniesiona na nowy plik migracji.
:::

## Ćwiczenie 7 - Asynchroniczność

Asynchrocznine operacje unikają blokowania wątku w momencie kiedy zapytanie jest przetwarzane przez baze danych. Asynchroniczne operacje są bardzo ważne aby utrzymać responsywność UI w aplikacjach klienckich oraz mogą znacznie zwiększyć przepustowość aplikacji Web.

EF Core naśladując .NET Standard zapewnia wsparcie asynchroniczności w wszystkich metodach, które wykonują operację IO. Owe operacja mają przynoszą ten sam efekt jak ich synchroniczne odpowiedniki i mogą być używane z słowę kluczowym `await` z języka C#. 

::: danger
EF Core nie zapewnia wsparcia wielowątkowości w oparciu o jedną instację klasy `DbContext`. Wszystkie operacje na dbContext powinny być wykonywane sekwencyjnie, jedna po drugiej. 
Zwykle wystarcza do tego słowo kluczowe `await`.
:::

::: warning
Asynchroniczna implementacja Microsoft.Data.SqlClient posiada znane problemy: 

- [https://github.com/dotnet/SqlClient/issues/593](https://github.com/dotnet/SqlClient/issues/593)
- [https://github.com/dotnet/SqlClient/issues/601](https://github.com/dotnet/SqlClient/issues/601)
:::
#### Przykładowy Zapis 

```csharp
var blog = new Blog { Url = "http://sample.com" };
context.Blogs.Add(blog);
await context.SaveChangesAsync();
```

#### Przykładowy Odczyt

```csharp
var groupedHighlyRatedBlogs = await context.Blogs
    .AsQueryable()
    .Where(b => b.Rating > 3) // server-evaluated
    .AsAsyncEnumerable()
    .GroupBy(b => b.Rating) // client-evaluated
    .ToListAsync();
```